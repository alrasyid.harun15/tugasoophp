<?php

class Harimau extends Hewan{
	
	public function __construct($nama){
		$this->nama = $nama;		
		$this->jumlahKaki = 4;
		$this->keahlian = "Lari Cepat";
		$this->attackPower = 7;
		$this->defencePower = 8;
	}

	public function getInfoHewan(){
		return "Harimau : " . parent::getInfoHewan(); 
	}

	public function atraksi():string{
		return get_class($this). " sedang lari cepat!!!!";
	}

}