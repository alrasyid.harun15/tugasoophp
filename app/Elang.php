<?php

class Elang extends Hewan{

	public function __construct($nama){
		$this->nama = $nama;		
		$this->jumlahKaki = 2;
		$this->keahlian = "Terbang Tinggi";
		$this->attackPower = 10;
		$this->defencePower = 5;
	}

	public function getInfoHewan(){
		return "Elang : " . parent::getInfoHewan(); 
	}

	public function atraksi():string{
		return get_class($this). " sedang terbang tinggi!!!!";
	}

}
