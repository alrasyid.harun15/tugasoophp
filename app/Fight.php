<?php

abstract class Fight{

	protected 	$attackPower, 
				$defencePower;

	private 	Hewan $target;			

	abstract public function serang(Hewan $target):string;

	abstract public function diserang(Hewan $target):string;

}