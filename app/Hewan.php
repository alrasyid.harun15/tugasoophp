<?php

abstract class Hewan extends Fight{

	protected 	$nama, 
				$darah = 50, 
				$jumlahKaki, 
				$keahlian;

	abstract public function atraksi():string;

	public function getNama(){
		return $this->nama;
	}

	public function getDarah(){
		return $this->darah;
	}

	public function setDarah($darah){
		return $this->darah = $darah;
	}

	public function getDefencePower(){
		return $this->defencePower;
	}

	public function getInfoHewan(){
		return "Nama hewan : {$this->nama}, HP : {$this->darah}, memiliki {$this->jumlahKaki} kaki, dan keahlian {$this->keahlian} | Atribut Fight > Attack Power : {$this->attackPower} & Defence Power : {$this->defencePower}";
	}

	public function serang(Hewan $target):string{

		return get_class($this) . " sedang menyerang " . get_class($target);

	}

	public function diserang(Hewan $attacker):string{

		$sisaDarah = $this->getDarah() - ($attacker->attackPower/$this->getDefencePower());
		$this->setDarah($sisaDarah);

		return get_class($this) . " sedang diserang " . get_class($attacker);
	}

}