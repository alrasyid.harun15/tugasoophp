<?php

abstract class Fight{

	protected 	$attackPower, 
				$defencePower;

	private 	Hewan $target;			

	abstract public function serang(Hewan $target):string;

	abstract public function diserang(Hewan $target):string;

}

abstract class Hewan extends Fight{

	protected 	$nama, 
				$darah = 50, 
				$jumlahKaki, 
				$keahlian;

	abstract public function atraksi():string;

	public function getNama(){
		return $this->nama;
	}

	public function getDarah(){
		return $this->darah;
	}

	public function setDarah($darah){
		return $this->darah = $darah;
	}

	public function getDefencePower(){
		return $this->defencePower;
	}

	public function getInfoHewan(){
		return "Nama hewan : {$this->nama}, HP : {$this->darah}, memiliki {$this->jumlahKaki} kaki, dan keahlian {$this->keahlian} | Atribut Fight > Attack Power : {$this->attackPower} & Defence Power : {$this->defencePower}";
	}

	public function serang(Hewan $target):string{

		return get_class($this) . " sedang menyerang " . get_class($target);

	}

	public function diserang(Hewan $attacker):string{

		$sisaDarah = $this->getDarah() - ($attacker->attackPower/$this->getDefencePower());
		$this->setDarah($sisaDarah);

		return get_class($this) . " sedang diserang " . get_class($attacker);
	}

}

class Harimau extends Hewan{
	
	public function __construct($nama){
		$this->nama = $nama;		
		$this->jumlahKaki = 4;
		$this->keahlian = "Lari Cepat";
		$this->attackPower = 7;
		$this->defencePower = 8;
	}

	public function getInfoHewan(){
		return "Harimau : " . parent::getInfoHewan(); 
	}

	public function atraksi():string{
		return get_class($this). " sedang lari cepat!!!!";
	}

}

class Elang extends Hewan{

	public function __construct($nama){
		$this->nama = $nama;		
		$this->jumlahKaki = 2;
		$this->keahlian = "Terbang Tinggi";
		$this->attackPower = 10;
		$this->defencePower = 5;
	}

	public function getInfoHewan(){
		return "Elang : " . parent::getInfoHewan(); 
	}

	public function atraksi():string{
		return get_class($this). " sedang terbang tinggi!!!!";
	}

}

$h_sumatera = new Harimau("Harimau Sumatera");
$e_jawa = new Elang("Elang Jawa");

echo "Start .... <br>";
echo $h_sumatera->atraksi();
echo "<br>";
echo $e_jawa->atraksi();
echo "<br>";

echo "Check Status Sebelum kelahi.... <hr>";
echo $h_sumatera->getInfoHewan();
echo "<br>";
echo $e_jawa->getInfoHewan();
echo "<br>";

$counter = 0;

while ( True ) {
	
	echo "<hr>Mulai Kelahi wave ".++$counter ."....<br>";
	echo $h_sumatera->serang($e_jawa);
	$val = rand();
	if($val%2==0 && $val/$val==1){
		echo "<br>Ceritanya gak kena .... <br>";
		echo $e_jawa->getInfoHewan();
		echo "<br>";	
	}else{
		echo "Ceritanya kena .... <br>";
		echo $e_jawa->diserang($h_sumatera);
		echo "<br>";
		echo $e_jawa->getInfoHewan();
		echo "<br>";	
	}

	if($e_jawa->getDarah()<0 || $h_sumatera->getDarah()<0){
		if($e_jawa->getDarah()>0){
			umumkanPemenang($e_jawa);
		}

		if($h_sumatera->getDarah()>0){
			umumkanPemenang($h_sumatera);
		}
		break;
	}

	echo "<hr>Mulai Kelahi wave ".++$counter." ....<br> ";
	echo $e_jawa->serang($h_sumatera);
	echo "<br>";
	if(rand()%2<>0){
		echo "<br>Ceritanya gak kena .... <br>";
		echo $h_sumatera->getInfoHewan();
		echo "<br>";	
	}else{
		echo "Ceritanya kena .... <br>";
		echo $h_sumatera->diserang($e_jawa);
		echo "<br>";
		echo $h_sumatera->getInfoHewan();
		echo "<br>";
	}	

	if($e_jawa->getDarah()<0 || $h_sumatera->getDarah()<0){
			if($e_jawa->getDarah()>0){
				umumkanPemenang($e_jawa);
			}

			if($h_sumatera->getDarah()>0){
				umumkanPemenang($h_sumatera);
			}
		break;
	}

}

function umumkanPemenang(Hewan $p1):void{
	if($p1->getDarah()>0){
		echo "<h1>Pemenang adalah : {$p1->getNama()}</h1>" ;
	}		
}