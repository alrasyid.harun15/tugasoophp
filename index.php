<?php

require_once 'init.php';

$h_sumatera = new Harimau("Harimau Sumatera");
$e_jawa = new Elang("Elang Jawa");

echo "Start .... <br>";
echo $h_sumatera->atraksi();
echo "<br>";
echo $e_jawa->atraksi();
echo "<br>";

echo "Check Status Sebelum kelahi.... <hr>";
echo $h_sumatera->getInfoHewan();
echo "<br>";
echo $e_jawa->getInfoHewan();
echo "<br>";

$counter = 0;

while ( True ) {
	
	echo "<hr>Mulai Kelahi wave ".++$counter ."....<br>";
	echo $h_sumatera->serang($e_jawa);
	$val = rand();
	if($val%2==0 && $val/$val==1){
		echo "<br>Ceritanya gak kena .... <br>";
		echo $e_jawa->getInfoHewan();
		echo "<br>";	
	}else{
		echo "Ceritanya kena .... <br>";
		echo $e_jawa->diserang($h_sumatera);
		echo "<br>";
		echo $e_jawa->getInfoHewan();
		echo "<br>";	
	}

	if($e_jawa->getDarah()<0 || $h_sumatera->getDarah()<0){
		if($e_jawa->getDarah()>0){
			umumkanPemenang($e_jawa);
		}

		if($h_sumatera->getDarah()>0){
			umumkanPemenang($h_sumatera);
		}
		break;
	}

	echo "<hr>Mulai Kelahi wave ".++$counter." ....<br> ";
	echo $e_jawa->serang($h_sumatera);
	echo "<br>";
	if(rand()%2<>0){
		echo "<br>Ceritanya gak kena .... <br>";
		echo $h_sumatera->getInfoHewan();
		echo "<br>";	
	}else{
		echo "Ceritanya kena .... <br>";
		echo $h_sumatera->diserang($e_jawa);
		echo "<br>";
		echo $h_sumatera->getInfoHewan();
		echo "<br>";
	}	

	if($e_jawa->getDarah()<0 || $h_sumatera->getDarah()<0){
			if($e_jawa->getDarah()>0){
				umumkanPemenang($e_jawa);
			}

			if($h_sumatera->getDarah()>0){
				umumkanPemenang($h_sumatera);
			}
		break;
	}

}

function umumkanPemenang(Hewan $p1):void{
	if($p1->getDarah()>0){
		echo "<h1>Pemenang adalah : {$p1->getNama()}</h1>" ;
	}		
}